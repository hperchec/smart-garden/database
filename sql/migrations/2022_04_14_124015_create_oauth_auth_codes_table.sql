--
-- Migration: create_oauth_auth_codes_table
--

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;

--
-- Description:
--
-- Create oauth_auth_codes table
--

DELIMITER //

--
-- UP
--

CREATE PROCEDURE UP ()

BEGIN

  -- Create `oauth_auth_codes` table
  CREATE TABLE `oauth_auth_codes` (

    -- Fields
    `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
    `user_id` bigint(20) unsigned NOT NULL,
    `client_id` bigint(20) unsigned NOT NULL,
    `scopes` text COLLATE utf8_unicode_ci,
    `revoked` tinyint(1) NOT NULL,
    `expires_at` datetime DEFAULT NULL,

    -- Keys
    PRIMARY KEY (`id`),
    KEY `oauth_auth_codes_user_id_index` (`user_id`)

  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

END//

--
-- DOWN
--

CREATE PROCEDURE DOWN ()

BEGIN

  -- Drop oauth_auth_codes table
  DROP TABLE `oauth_auth_codes`;

END//

DELIMITER ;

--

-- ! Don't remove these lines !
SET @query = CONCAT('CALL ', @PROC, '()');
PREPARE stmt FROM @query; EXECUTE stmt;
-- ! Don't remove these lines !

-- Reset global variables here

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;