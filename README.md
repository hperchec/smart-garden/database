# 🍃 *Smart Garden* (Database) 🌐

[![smart-garden](https://img.shields.io/static/v1?label=&message=Smart%20Garden&color=191919&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACkElEQVQ4T3VTTUhUURQ+Z37fjFpMmFmr3LZuU4sKyqKgRWZ/IES1MIICNRlTsgchpowWA0ltLFKohqJ9EGVB1KZNFAVqfxIWVI5vft57c+89nfucNw1iFw7vncv5vvOdn4vwnzNG73cRFEcFWMoGqyuJ+5+sFIrLL+/QfJMLdqoEVgsb+CYg90hA4byJZ2arMRWCDFGtA9YFAfnOEiwaPtCFRWAwE+UAyHEiUl4LhNyBJCYtTeQR3CM6qkCMMHADWzmr/9UqciChsBBSIm1I1R+Tcp6t+2RtzyTeJAqvArI5IKCzLRH44NwPVvSCCZ4TZO+b2P0z7YzMGlI2xaVw2+p6o5gmitZDyfaBZcnPJNi9HbDzFSBSdc037OGMIdShuJR0eHVfALn2CNfu+ASccSoE2NyOm0s+0CQzbqJZ0P54fuh2VMrjTAAHEhcRb9EnA6Gm6MsWUNxxDrdO6WCTrjcCqE38+8HEs9/13UTuypQhxDZNsG/NpSUFFsxVKbCbHPimXFCnFNhvTTj9kFvtlTGRH1gflPiFweG4kNBcD0GPYAGmywSWHtk4N+2PA9l+E9s92d4hwMn84F1DyCM6uyaghkCYCd5FfoNy3PLScA+GktjSU924sYXBRDwSTEdLpTYPXLaslTXQJAqthTdaQaAEhccK5lsdEK+JnK+GUjM8snVsuxlU52fmEUKUSH399THmLVKaXh5zIZuSkJuxYXGa38CJkJLAQOCF8czPWqN9JecQsWtLw+VMZZVNytSGwe3hHnQiuTFeWQ5U/8BCaL/I4JQjC0N7GlP5yipX12vS1Y0RwmFDUiuPCyvZlXwQAtW9N2F+XvExLX+V6eLw9hjBKBMEY0J1HEz0PV0eo/2/bjSnwFQoTgQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/smart-garden)
[![app-database](https://img.shields.io/static/v1?labelColor=191919&label=database&message=v1.0.0&color=191919&logo=data:image/ico;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD///8A////Hv///2L///+h////zf///+j////3/////v////7////3////6P///83///+h////Yv///x////8A////av///9L////6//////////////////////////////////////////////////////////r////S////av////f///////////////////////////////////////////////////////////////////////////////f//////////////+/////Q////sP///5b///+G////fv///37///+G////lv///7D////Q////7///////////////xP///3T///9B////Pf///07///9i////cv///3r///96////cv///2L///9O////Pf///0H///90////xf///y3///9v////vf///+b////4////////////////////////////////////+P///+b///+9////b////y3////F///////////////////////////////////////////////////////////////////////////////F////////////////////////////////////////////////////////////////////////////////////////////////////7////9D///+w////lv///4b///9+////fv///4b///+W////sP///9D////v///////////////E////dP///0H///89////Tv///2L///9y////ev///3r///9y////Yv///07///89////Qf///3T////F////Lf///2////+9////5v////j////////////////////////////////////4////5v///73///9v////Lf///8X//////////////////////////////////////////////////////////////////////////////8X/////////////////////////////////////////////////////////////////////////////////////////9///////////////////////////////////////////////////////////////////////////////9////2r////S////+v/////////////////////////////////////////////////////////6////0v///2r///8A////Hv///2L///+h////zf///+j////3/////v////7////3////6P///83///+h////Yv///x////8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==)](https://gitlab.com/hperchec/smart-garden/database)
[![scorpion](https://img.shields.io/static/v1?label=&message=Scorpion&color=362F2D&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACWklEQVQ4T4WSTUhUURTH753PN2MTRZAtAinKdiGt2ojbCAIrmEghU4i+aMApyU3ja2aYmaaNOBLRQl0UUUESgSESDhTkJiRKTFv0gVEkBco0737MPaf7RuerUu/q8e45v3v+//+hpOoszya2+Ti5TySZcS1u6qWHQ7z6/n/ftPSTzSd3OyUdIxz2EQaEcJyGnGrzHjHfrwcpAwqzybsoSftKM8wgUxOEkS5lFZp8wfjHtSAVwLtEBwoyYgOQawiDTuDwkwhsMIKxwQ0BOGVuLjjcP/TrXrSnYAgoVMrz1tlHTbOwIcAukK/i87p5r262ZRAbRBlmJYe2urOJb+uaWAS8iH1HhvV2sy0FGC5QrqaIBQe1nNO+y+nnf0PKHtgXIhvjutFT9KEkg5NG+lueQIlRtFTUIIG4lqRfWDllAI7frJNOKwcWXHJwyKyOn3crdwP/tbSFKNeHIpTjhMFkO81kFmsBk+ZOyelrz6G+evEgcpEwdZwKfOk+k4jYhez6lWW0IGBPRwV5Ytzqb60B5J6Z+z2KvEEBQe+x6KNqrcwMN6Kic9qLojc62mHfnYGuGoAcu9aC0pnVC/QVODb7TlWWJ2f27HBx+KwlfNE+7NEmX/UPD6ZrAPyp2UoFjK6aJwhXEe+F1I3SJFZPdwvmIUwENFPpPOAb6f9UAxCPI53a6aHiiAxQ74LwgGMX7a7knz8fmlachANDA5P/pCAemk0gCuOU43Y9ogCuEsaSP6kjE6Xi/LnQUf/tgdFqf2r2AO/1buU5R4oyOKnjcusktKmODiOenltrlf8Awt9jILDjUAQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/boilerplates/scorpion)
[![Docker](https://shields.io/static/v1?logo=docker&logoColor=white&label=&message=Docker&color=2496ED)](https://docker.com/)
[![MySQL](https://shields.io/static/v1?logo=mysql&logoColor=white&label=&labelColor=F29221&message=MySQL%205.7&color=3E6E93)](https://mysql.com/)
[![pipeline status](https://gitlab.com/hperchec/smart-garden/app/database/badges/main/pipeline.svg)](https://gitlab.com/hperchec/smart-garden/database/commits/main)

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

> This project is based on [Scorpion](https://gitlab.com/hperchec/boilerplates/scorpion/app/database) boilerplate **database** project

**Table of contents**:

[[_TOC_]]

## Get started

> **IMPORTANT**: Please refer to the [project documentation](https://gitlab.com/hperchec/smart-garden/docs).

Clone this repository:

```bash
git clone https://gitlab.com/hperchec/smart-garden/database.git
```

----

Made with ❤ by [**Hervé Perchec**](http://herve-perchec.com/)
